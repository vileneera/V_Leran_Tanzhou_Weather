<%@ page language="java" contentType="text/html; charset=UTF-8"  
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<!-- 当前页面的三要素-->
		<title>java开发天气预报</title>
		<meta name="Keywords" content="关键词,关键词">
		<meta name="description" content="">
		<style type="text/css">
		body{font-size: 14px;font-family: Microsoft Yahei;
			padding-left: 10px;
		}
		#air{width: 600px;height: 500px;
			background: #0099cc;
		}
		</style>
	</head>
	<body>
		<div id="air">
		 	城市:${weather.city}<br/>
		 	信息获取时间: ${weather.date}
		 	<br/>
		 	<c:forEach items="${weather.results}" var="day">
		 		${day.date} <br/>
		 		白天:<img src="${day.dayPictureUrl}" />
		 		晚上:<img src="${day.nightPictureUrl}" />
		 		温度:${day.temperature}
		 		<hr/>
		 	</c:forEach>
		</div>
			
	</body>
</html>
