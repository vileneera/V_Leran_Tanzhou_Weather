package com.vilen.model;

import java.util.List;

/**
 * Created by vilen on 2015/4/2.
 */
public class ResultModel {
    private String error;//错误代码
    private String status;//状态
    private String date;//日期
    private String city;
    private List<Weather> results;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Weather> getResults() {
        return results;
    }

    public void setResults(List<Weather> results) {
        this.results = results;
    }
}
