package com.vilen.model;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vilen on 2015/4/2.
 */
public class JsonTest extends TestCase{
    @Test
    public void testjson(){
        String rs= "{\"error\":0,\"status\":\"success\",\"date\":\"2015-04-02\",\"results\":[{\"currentCity\":\"库尔勒\",\"pm25\":\"500\",\"index\":[{\"title\":\"穿衣\",\"zs\":\"较舒适\",\"tipt\":\"穿衣指数\",\"des\":\"建议着薄外套、开衫牛仔衫裤等服装。年老体弱者应适当添加衣物，宜着夹克衫、薄毛衣等。\"},{\"title\":\"洗车\",\"zs\":\"较适宜\",\"tipt\":\"洗车指数\",\"des\":\"较适宜洗车，未来一天无雨，风力较小，擦洗一新的汽车至少能保持一天。\"},{\"title\":\"旅游\",\"zs\":\"适宜\",\"tipt\":\"旅游指数\",\"des\":\"天气较好，风稍大，但温度适宜，总体来说还是好天气。这样的天气适宜旅游，您可以尽情享受大自然的风光。\"},{\"title\":\"感冒\",\"zs\":\"较易发\",\"tipt\":\"感冒指数\",\"des\":\"天凉，昼夜温差较大，较易发生感冒，请适当增减衣服，体质较弱的朋友请注意适当防护。\"},{\"title\":\"运动\",\"zs\":\"较适宜\",\"tipt\":\"运动指数\",\"des\":\"阴天，较适宜进行各种户内外运动。\"},{\"title\":\"紫外线强度\",\"zs\":\"最弱\",\"tipt\":\"紫外线强度指数\",\"des\":\"属弱紫外线辐射天气，无需特别防护。若长期在户外，建议涂擦SPF在8-12之间的防晒护肤品。\"}],\"weather_data\":[{\"date\":\"周四 04月02日 (实时：10℃)\",\"dayPictureUrl\":\"http://api.map.baidu.com/images/weather/day/fuchen.png\",\"nightPictureUrl\":\"http://api.map.baidu.com/images/weather/night/duoyun.png\",\"weather\":\"浮尘转多云\",\"wind\":\"北风3-4级\",\"temperature\":\"19 ~ 10℃\"},{\"date\":\"周五\",\"dayPictureUrl\":\"http://api.map.baidu.com/images/weather/day/fuchen.png\",\"nightPictureUrl\":\"http://api.map.baidu.com/images/weather/night/duoyun.png\",\"weather\":\"浮尘转多云\",\"wind\":\"北风3-4级\",\"temperature\":\"14 ~ 6℃\"},{\"date\":\"周六\",\"dayPictureUrl\":\"http://api.map.baidu.com/images/weather/day/qing.png\",\"nightPictureUrl\":\"http://api.map.baidu.com/images/weather/night/qing.png\",\"weather\":\"晴\",\"wind\":\"微风\",\"temperature\":\"13 ~ 7℃\"},{\"date\":\"周日\",\"dayPictureUrl\":\"http://api.map.baidu.com/images/weather/day/duoyun.png\",\"nightPictureUrl\":\"http://api.map.baidu.com/images/weather/night/qing.png\",\"weather\":\"多云转晴\",\"wind\":\"微风\",\"temperature\":\"18 ~ 8℃\"}]}]}";

        ResultModel rm = JSONObject.parseObject(rs,ResultModel.class);
//        System.out.println(rm.getDate());
        JSONArray jsonResult = JSON.parseObject(rs).getJSONArray("results");

        List<Weather> wList  = new ArrayList<Weather>();
        for (int i = 0; i < jsonResult.size(); i++) {
            JSONArray jsonWeatherData = jsonResult.getJSONObject(i).getJSONArray("weather_data");
            List<Weather> weathers = JSONObject.parseArray(jsonWeatherData.toJSONString(), Weather.class);
            wList.addAll(weathers);
        }
//        System.out.println(wList);
        rm.setResults(wList);

    }

}
